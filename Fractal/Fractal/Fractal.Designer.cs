﻿namespace Fractal
{
    partial class Fractal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fractal));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.file_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_reload = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_saveState = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_loadState = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_saveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_palette = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_blue = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_brown = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_green = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_orange = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_pink = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.subMenuItem_default = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_colourCycle = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_on = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuItem_off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_help = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_about = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.ColourCycle = new System.Windows.Forms.Timer(this.components);
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.file_menu,
            this.menu_help});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(663, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // file_menu
            // 
            this.file_menu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_reload,
            this.menuItem_saveState,
            this.menuItem_loadState,
            this.menuItem_saveAs,
            this.menuItem_palette,
            this.menuItem_colourCycle});
            this.file_menu.Name = "file_menu";
            this.file_menu.Size = new System.Drawing.Size(37, 20);
            this.file_menu.Text = "File";
            // 
            // menuItem_reload
            // 
            this.menuItem_reload.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_reload.Image")));
            this.menuItem_reload.Name = "menuItem_reload";
            this.menuItem_reload.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.menuItem_reload.Size = new System.Drawing.Size(189, 22);
            this.menuItem_reload.Text = "Reload";
            this.menuItem_reload.Click += new System.EventHandler(this.menuItem_reload_Click);
            // 
            // menuItem_saveState
            // 
            this.menuItem_saveState.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_saveState.Image")));
            this.menuItem_saveState.Name = "menuItem_saveState";
            this.menuItem_saveState.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuItem_saveState.Size = new System.Drawing.Size(189, 22);
            this.menuItem_saveState.Text = "Save State";
            this.menuItem_saveState.Click += new System.EventHandler(this.menuItem_saveState_Click);
            // 
            // menuItem_loadState
            // 
            this.menuItem_loadState.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_loadState.Image")));
            this.menuItem_loadState.Name = "menuItem_loadState";
            this.menuItem_loadState.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.menuItem_loadState.Size = new System.Drawing.Size(189, 22);
            this.menuItem_loadState.Text = "Load State";
            this.menuItem_loadState.Click += new System.EventHandler(this.menuItem_loadState_Click);
            // 
            // menuItem_saveAs
            // 
            this.menuItem_saveAs.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_saveAs.Image")));
            this.menuItem_saveAs.Name = "menuItem_saveAs";
            this.menuItem_saveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.menuItem_saveAs.Size = new System.Drawing.Size(189, 22);
            this.menuItem_saveAs.Text = "Save As ...";
            this.menuItem_saveAs.Click += new System.EventHandler(this.menuItem_saveAs_Click);
            // 
            // menuItem_palette
            // 
            this.menuItem_palette.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuItem_blue,
            this.subMenuItem_brown,
            this.subMenuItem_green,
            this.subMenuItem_orange,
            this.subMenuItem_pink,
            this.toolStripSeparator1,
            this.subMenuItem_default});
            this.menuItem_palette.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_palette.Image")));
            this.menuItem_palette.Name = "menuItem_palette";
            this.menuItem_palette.Size = new System.Drawing.Size(189, 22);
            this.menuItem_palette.Text = "Colour Palette";
            // 
            // subMenuItem_blue
            // 
            this.subMenuItem_blue.BackColor = System.Drawing.Color.White;
            this.subMenuItem_blue.ForeColor = System.Drawing.Color.Black;
            this.subMenuItem_blue.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_blue.Image")));
            this.subMenuItem_blue.Name = "subMenuItem_blue";
            this.subMenuItem_blue.Size = new System.Drawing.Size(113, 22);
            this.subMenuItem_blue.Text = "Blue";
            this.subMenuItem_blue.Click += new System.EventHandler(this.subMenuItem_blue_Click);
            // 
            // subMenuItem_brown
            // 
            this.subMenuItem_brown.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_brown.Image")));
            this.subMenuItem_brown.Name = "subMenuItem_brown";
            this.subMenuItem_brown.Size = new System.Drawing.Size(113, 22);
            this.subMenuItem_brown.Text = "Brown";
            this.subMenuItem_brown.Click += new System.EventHandler(this.subMenuItem_brown_Click);
            // 
            // subMenuItem_green
            // 
            this.subMenuItem_green.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_green.Image")));
            this.subMenuItem_green.Name = "subMenuItem_green";
            this.subMenuItem_green.Size = new System.Drawing.Size(113, 22);
            this.subMenuItem_green.Text = "Green";
            this.subMenuItem_green.Click += new System.EventHandler(this.subMenuItem_green_Click);
            // 
            // subMenuItem_orange
            // 
            this.subMenuItem_orange.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_orange.Image")));
            this.subMenuItem_orange.Name = "subMenuItem_orange";
            this.subMenuItem_orange.Size = new System.Drawing.Size(113, 22);
            this.subMenuItem_orange.Text = "Orange";
            this.subMenuItem_orange.Click += new System.EventHandler(this.subMenuItem_orange_Click);
            // 
            // subMenuItem_pink
            // 
            this.subMenuItem_pink.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_pink.Image")));
            this.subMenuItem_pink.Name = "subMenuItem_pink";
            this.subMenuItem_pink.Size = new System.Drawing.Size(113, 22);
            this.subMenuItem_pink.Text = "Pink";
            this.subMenuItem_pink.Click += new System.EventHandler(this.subMenuItem_pink_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(110, 6);
            // 
            // subMenuItem_default
            // 
            this.subMenuItem_default.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_default.Image")));
            this.subMenuItem_default.Name = "subMenuItem_default";
            this.subMenuItem_default.Size = new System.Drawing.Size(113, 22);
            this.subMenuItem_default.Text = "Default";
            this.subMenuItem_default.Click += new System.EventHandler(this.subMenuItem_default_Click);
            // 
            // menuItem_colourCycle
            // 
            this.menuItem_colourCycle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuItem_on,
            this.subMenuItem_off});
            this.menuItem_colourCycle.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_colourCycle.Image")));
            this.menuItem_colourCycle.Name = "menuItem_colourCycle";
            this.menuItem_colourCycle.Size = new System.Drawing.Size(189, 22);
            this.menuItem_colourCycle.Text = "Colour Cycle";
            // 
            // subMenuItem_on
            // 
            this.subMenuItem_on.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_on.Image")));
            this.subMenuItem_on.Name = "subMenuItem_on";
            this.subMenuItem_on.Size = new System.Drawing.Size(91, 22);
            this.subMenuItem_on.Text = "On";
            this.subMenuItem_on.Click += new System.EventHandler(this.subMenuItem_on_Click);
            // 
            // subMenuItem_off
            // 
            this.subMenuItem_off.Image = ((System.Drawing.Image)(resources.GetObject("subMenuItem_off.Image")));
            this.subMenuItem_off.Name = "subMenuItem_off";
            this.subMenuItem_off.Size = new System.Drawing.Size(91, 22);
            this.subMenuItem_off.Text = "Off";
            this.subMenuItem_off.Click += new System.EventHandler(this.subMenuItem_off_Click);
            // 
            // menu_help
            // 
            this.menu_help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_about});
            this.menu_help.Name = "menu_help";
            this.menu_help.Size = new System.Drawing.Size(44, 20);
            this.menu_help.Text = "Help";
            // 
            // menuItem_about
            // 
            this.menuItem_about.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_about.Image")));
            this.menuItem_about.Name = "menuItem_about";
            this.menuItem_about.Size = new System.Drawing.Size(107, 22);
            this.menuItem_about.Text = "About";
            this.menuItem_about.Click += new System.EventHandler(this.menuItem_about_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 27);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(663, 489);
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // ColourCycle
            // 
            this.ColourCycle.Interval = 50;
            this.ColourCycle.Tick += new System.EventHandler(this.ColourCycle_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 516);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mandelbrot Set";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem file_menu;
        private System.Windows.Forms.ToolStripMenuItem menuItem_reload;
        private System.Windows.Forms.ToolStripMenuItem menuItem_saveState;
        private System.Windows.Forms.ToolStripMenuItem menu_help;
        private System.Windows.Forms.ToolStripMenuItem menuItem_about;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ToolStripMenuItem menuItem_saveAs;
        private System.Windows.Forms.ToolStripMenuItem menuItem_loadState;
        private System.Windows.Forms.ToolStripMenuItem menuItem_palette;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_blue;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_brown;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_orange;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_pink;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_green;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_default;
        private System.Windows.Forms.Timer ColourCycle;
        private System.Windows.Forms.ToolStripMenuItem menuItem_colourCycle;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_on;
        private System.Windows.Forms.ToolStripMenuItem subMenuItem_off;
    }
}


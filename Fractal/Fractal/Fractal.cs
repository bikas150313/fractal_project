﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Fractal
{
    public partial class Fractal : Form
    {

        //instance variables declaration
        private int MAX = 256;      // max iterations
        private double SX = -2.025; // start value real
        private double SY = -1.125; // start value imaginary
        private double EX = 0.6;    // end value real
        private double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle;
        private static float xy;
        private bool mouseDown = false;
        private bool stateSaved = false;
        private Bitmap image;
        private Graphics graphics;
        private Cursor c1;
        private HSB hsb;
        private int colourCode = 0;
        private bool cycleForward = true;

        public Fractal()
        {
            //initialize form components
            InitializeComponent();
            hsb = new HSB();
            c1 = Cursors.Cross;
            x1 = pictureBox.Width;
            y1 = pictureBox.Height;
            xy = (float)x1 / (float)y1;
            image = new Bitmap(pictureBox.Width, pictureBox.Height);
            graphics = Graphics.FromImage(image);
            Start();
        }

        //event triggered on Reload menu click
        private void menuItem_reload_Click(object sender, EventArgs e)
        {
            stateSaved = false;
            pictureBox.Image = null;
            Start();
        }

        //event triggered on Save State menu click
        private void menuItem_saveState_Click(object sender, EventArgs e)
        {
            stateSaved = true;
            SaveState();
        }

        //function to save Mandelbrot state
        public void SaveState()
        {
            //try and catch exception
            try
            {
                //creates a xml file named state.xml 
                //and saves the current values for xstart, ystart, xzoom and yzoom 
                XmlWriter writer = XmlWriter.Create("state.xml");
                writer.WriteStartDocument();
                writer.WriteStartElement("states");
                writer.WriteElementString("xstart", xstart.ToString());
                writer.WriteElementString("ystart", ystart.ToString());
                writer.WriteElementString("xzoom", xzoom.ToString());
                writer.WriteElementString("yzoom", yzoom.ToString());
                writer.WriteEndElement();
                writer.WriteEndDocument();
                //flush from memory
                writer.Flush();
                //close XMLWriter
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //event triggered on Load State menu click
        private void menuItem_loadState_Click(object sender, EventArgs e)
        {
            stateSaved = true;
            LoadState();
        }

        //function to load Mandelbrot state
        public void LoadState()
        {
            pictureBox.Image = null;
            //checks for file existence
            String exists = "state.xml";
            if (File.Exists(exists))
            {
                //try and catch exception
                try
                {
                    //loads and assigns the saved values for xstart, ystart, xzoom and yzoom
                    XmlDocument state = new XmlDocument();
                    state.Load("state.xml");
                    foreach (XmlNode node in state)
                    {
                        xstart = Convert.ToDouble(node["xstart"]?.InnerText);
                        ystart = Convert.ToDouble(node["ystart"]?.InnerText);
                        xzoom = Convert.ToDouble(node["xzoom"]?.InnerText);
                        yzoom = Convert.ToDouble(node["yzoom"]?.InnerText);
                    }
                    //reform Mandelbrot image
                    MandelbrotSet();
                    Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("No previously saved state found!");
            }
        }

        //event triggered on Blue menu click
        private void subMenuItem_blue_Click(object sender, EventArgs e)
        {
            //colour code for Blue colour
            colourCode = -100;
            //reform Mandelbrot image
            MandelbrotSet();
            Refresh();
        }

        //event triggered on Brown menu click
        private void subMenuItem_brown_Click(object sender, EventArgs e)
        {
            //colour code for Brown colour
            colourCode = -250;
            //reform Mandelbrot image
            MandelbrotSet();
            Refresh();
        }

        //event triggered on Orange menu click
        private void subMenuItem_orange_Click(object sender, EventArgs e)
        {
            //colour code for Orange colour
            colourCode = 11;
            //reform Mandelbrot image
            MandelbrotSet();
            Refresh();
        }

        //event triggered on Pink menu click
        private void subMenuItem_pink_Click(object sender, EventArgs e)
        {
            //colour code for Pink colour
            colourCode = -10;
            //reform Mandelbrot image
            MandelbrotSet();
            Refresh();
        }

        //event triggered on Green menu click
        private void subMenuItem_green_Click(object sender, EventArgs e)
        {
            //colour code for Green colour
            colourCode = -170;
            //reform Mandelbrot image
            MandelbrotSet();
            Refresh();
        }

        //event triggered on Default menu click
        private void subMenuItem_default_Click(object sender, EventArgs e)
        {
            //colour code for Default colour
            colourCode = 0;
            //reform Mandelbrot image
            MandelbrotSet();
            Refresh();
        }

        //function called when timer is active
        private void ColourCycle_Tick(object sender, EventArgs e)
        {
            //reforms Mandelbrot image
            MandelbrotSet();
            Refresh(); 
            //when colourCode is 0 i.e. default Mandelbrot image
            //then colour cycles backwards
            if (colourCode == 0)
            {
                cycleForward = false;
            }
            //when colourCode is -250 i.e. Brown Mandelbrot image
            //then colour cycles forwards
            if (colourCode == -250)
            {
                cycleForward = true;
            }
            //when value is true, colourCode increases with 2 points
            if (cycleForward)
            {
                colourCode=colourCode+2; 
            }
            //else colourCode decreases with 2 points
            else
            {
                colourCode=colourCode-2; 
            }
        }

        //event triggered on On menu click
        private void subMenuItem_on_Click(object sender, EventArgs e)
        {
            //starts timer
            ColourCycle.Start();
        }

        //event triggered on Off menu click
        private void subMenuItem_off_Click(object sender, EventArgs e)
        {
            //stops timer
            ColourCycle.Stop();
            ColourCycle.Dispose();
        }

        //event triggered when form opens
        private void Form1_Shown(object sender, EventArgs e)
        {
            //dialog asks the user to load previously saved state or not
            DialogResult result = MessageBox.Show("Do you want to load previously saved state?", "Load State?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //when user click yes previously saved state is loaded
            if (result == DialogResult.Yes)
            {
                stateSaved = true;
                LoadState();
            }
        }

        //event triggered while form is closing
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if value is false
            if (!stateSaved)
            {
                //dialog asks the user to save current state or not
                DialogResult result = MessageBox.Show("Do you want to save the current state?", "Save State?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                //when user click yes current state is saved
                if (result == DialogResult.Yes)
                {
                    SaveState();
                }
                else if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        //event triggered on Save as menu click
        private void menuItem_saveAs_Click(object sender, EventArgs e)
        {
            //opens save file dialog
            SaveFileDialog saveImage = new SaveFileDialog();
            //filtering file extension
            saveImage.Filter = "PNG Image(*.png) | *.png | JPG Image(*.jpg) | *.jpg | BMP Image(*.bmp) | *.bmp";
            //when user click ok mandelbrot image is saved
            if (saveImage.ShowDialog() == DialogResult.OK)
            {
                image.Save(saveImage.FileName);
            }
        }

        //event triggered on about menu click
        private void menuItem_about_Click(object sender, EventArgs e)
        {
            //opens 'About' form
            About about = new About();
            about.ShowDialog();
        }

        //starts calculating all the points and forming Mandelbrot image
        public void Start()
        {
            action = false;
            rectangle = false;
            InItValues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            MandelbrotSet();
        }

        //assigns values for xstart, ystart, xende and yende
        public void InItValues()
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
            {
                xstart = xende - (yende - ystart) * (double)xy;
            }
        }

        //event triggered on pictureBox paint event
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            Image tempPic = Image.FromHbitmap(image.GetHbitmap());
            Graphics g = Graphics.FromImage(tempPic);
            //draws a rectangle into the picture box on mouse drag
            if (rectangle)
            {
                Pen pen = new Pen(Color.White);
                Rectangle rect;
                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xs, ys, (xe - xs), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                    }
                }
                g.DrawRectangle(pen, rect);
                pictureBox.Image = tempPic;
            }
        }

        //calculates all points and draws the Mandlebrot image
        public void MandelbrotSet()
        {
            int x, y;
            float h, b, alt = 0.0f;
            Pen pen = new Pen(Color.White);
            for (x = 0; x < x1; x += 2)
            {
                for (y = 0; y < y1; y++)
                {
                    h = PointColor(xstart + xzoom * (double)x, ystart + yzoom * (double)y);

                    if (h != alt)
                    {
                        b = 1.0f - h * h;
                        //passing hue, saturation and brightness value to HSB class
                        hsb.FromHSB(h, 0.8f, b);
                        //gets RGB colour structure from HSB class
                        Color col = Color.FromArgb(Convert.ToByte(hsb.rChan), Convert.ToByte(hsb.gChan), Convert.ToByte(hsb.bChan));
                        pen = new Pen(col);
                        alt = h;
                    }
                    //drawing pixel
                    graphics.DrawLine(pen, new Point(x, y), new Point(x + 1, y)); 
                }
            }
            //sets pictureBox image
            pictureBox.Image = image;
            pictureBox.Cursor = c1;
            action = true;
        }

        //returns a colourCode
        //helped in colour palette and colour cycling
        public float PointColor(double xwert, double ywert)
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = colourCode;
            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i; 
                i = 2.0 * r * i + ywert; 
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        //event triggered on mouse press
        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            //stores current x-cordinate and y-cordinate
            if (action)
            {
                mouseDown = true;
                xs = e.X;
                ys = e.Y;
            }
        }

        //event triggered on mouse move
        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            //updates and stores current x-cordinate and y-cordinate on mouse move
            if (action && mouseDown)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                pictureBox.Refresh();
            }
        }

        //event triggered on mouse release
        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            stateSaved = false;
            int z, w;
            //code to zoom Mandelbrot image
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2))
                {
                    InItValues();
                }
                else
                {
                    if (((float)w > (float)z * xy))
                        ye = (int)((float)ys + (float)w / xy);
                    else
                        xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                MandelbrotSet();
                rectangle = false;
                pictureBox.Refresh();
                mouseDown = false;
            }
        }
    }
}
